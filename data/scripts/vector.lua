local Vector = {}
do
    local meta = {
        _metatable = "Private metatable",
        _DESCRIPTION = "Vectors in 2D"
    }

    meta.__index = meta

    function meta:__add( v )
        return Vector(self.x + v.x, self.y + v.y)
    end
    
    function meta:__sub( v )
        return Vector(self.x - v.x, self.y - v.y)
    end

    function meta:__mul( v )
      if type(v) == 'number' then
        return Vector(self.x*v, self.y*v)
      elseif type(self) == "number" then
        return Vector(v.x*self, v.y *self)
      else
        return self.x * v.x + self.y * v.y
      end
    end

    function meta:__tostring()
        return ("<%g, %g>"):format(self.x, self.y)
    end

    function meta:length_squared()
      return self * self
    end

    function meta:length()
        return math.sqrt(self:length_squared())
    end
    
    function meta:normalized()
      local fac = 1.0 / self:length()
      return Vector(self.x * fac, self.y * fac)
    end
    
    function meta:normalize()
      local fac = 1.0 / self:length()
      self.x, self.y = self.x * fac, self.y * fac
      return self
    end
    
    function meta:angle()
      return math.atan2(-self.y, self.x)
    end

    setmetatable( Vector, {
        __call = function(V, x, y) return setmetatable( {x = x, y = y}, meta ) end
    } )
end

Vector.__index = Vector

return Vector