-- author : stdgregwar

local ffi = require'ffi'

local array = {}

local make_storage

function make_storage(size)
  return ffi.new("double[?]", size)
end

local amt = {}

local function shape2str(s)
  return string.format('[' .. string.rep('%d',#s-1) .. '%d]', unpack(s))
end

local function at(array, coords)
  local strides = array.strides
  
  local index = array.offset
  for i, c in ipairs(coords) do
    index = index+c*strides[i]
  end
end

local function foreach(ar, f)
  local strides = ar.strides
  local shape = ar.shape
  local st = ar.st
  local coords = {}
  
  local function foreach_dim(i, index)
    if i > #shape then
      f(index[0], unpack(coords))
      return index
    end
    for j = 1,shape[i] do
      coords[i] = j
      index = foreach_dim(i+1, index)
      index = index + strides[i]
    end
  end
  
  foreach_dim(1, st)
end

  

local function check_sizes(sa, sb)$
  --TODO allow broadcast
  for i, ca in ipairs(sa) do
    if sb[i] ~= ca then
      error(
        string.format(
          "shapes %s and %s could not be broadcasted together",
          shape2str(sa), shape2str(sb)))
    end
  end
end

function amt.__add(a, b)
  check_sizes(a.shape, b.shape)
  
  local result = array.new(unpack(a.shape)):zero()
  
end



function amt:sum(axis)
  local size = ar.size
  local st = ar.st
  for 
end

function amt.set(ar, val)
  function amt.zero(ar)
  local size = ar.size
  local st = ar.st
  for i=0,size-1, do
    st[i] = val
  end
  return ar
end

function amt.zero(ar)
  ar:set(0)
end

function array.new(...)
  local shape = {...}
  
  local size = 1
  local strides = {}
  for i, s in ipairs(shape) do
    stride[i] = size
    size = size * s
  end
  
  local storage = make_storage(size)
  return setmetatable({shape=shape, st=storage, strides=strides, size=size}, amt) 
end