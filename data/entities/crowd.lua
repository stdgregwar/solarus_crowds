-- Lua script of custom entity crowd.
-- This script is executed every time a custom entity with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

-- @author : stdgregwar

local Vec = require'scripts/vector'

local entity = ...
local game = entity:get_game()
local map = entity:get_map()
local hero = map:get_hero()

local ents = {}

local count = tonumber(entity:get_property("count") or 20)
local generator = entity:get_property("generator") or "npc/woman_1[a,b,c,d,e]"

local fixed, list = generator:match("(.*)%[(.*)%]")
local tocks = {}

local arrow = sol.sprite.create'menus/arrow'
arrow:set_direction(0)

for t in list:gmatch("[^,]+") do
  table.insert(tocks, t)
end
 
print(fixed, list, #tocks)

local function sprite_gen()
  return fixed .. tocks[math.random(1,#tocks)]
end


-- Event called when the custom entity is initialized.
function entity:on_created()

  -- Initialize the properties of your custom entity here,
  -- like the sprite, the size, and whether it can traverse other
  -- entities and be traversed by them.
  
  -- get bounds of the custom entity representing the crowd
  local l, t, w, h = entity:get_bounding_box()
  local ex, ey, el = entity:get_position()
  
  -- spawn child entities into the bounds
  -- entity count is controlled by the "count" property
  for i=1,count do
    local x = math.random(l, l+w)
    local y = math.random(t, t+h)
    local ent = map:create_npc{
      layer = el,
      x = x,
      y = y,
      subtype = 1,
      direction = 0,
      sprite = sprite_gen()
    }
    local mov = sol.movement.create("target")
    mov:start(ent)
    ent.pos = Vec(x, y)
    ent.speed = Vec(0, 0)
--    function ent:on_movement_changed(mov)
--      mov:set_angle(ent.angle)
--      mov:set_speed(ent.speed)
--    end
    ents[i] = ent
  end
  
  -- transform this entity
  entity:set_size(16,16)
  local mov = sol.movement.create('random')
  entity:set_can_traverse("npc", true)
  --mov:start(entity)
end

local l_length = 40
local repulse = 2000

local crit_dist = 50

local function elastic_force(dist, dir, l)
  local elastic = dist-l
  return dir * elastic
end

local function coulomb_force(dist2, dir, fac)
  local coulomb = fac / math.max(dist2,1e-2)
  return dir * coulomb
end

local function driving_force(opos, pos, elen, rep, elw)
  local r = opos - pos
  local dist2 = r:length_squared()
  local dist = math.sqrt(dist2)
  r = r:normalize()
  return elastic_force(dist, r, elen) +
  coulomb_force(dist2, r, rep)
end

local function hero_force(hpos, pos)
  local r = hpos - pos
  local dist2 = r:length_squared()
  r = r:normalize()
  if dist2 > crit_dist*crit_dist then
    return Vec(0,0)
  else
    return coulomb_force(dist2, r, repulse)
  end
end

local function get_crowd_forces(i, pos)
  local e = Vec(0, 0)
  local c = Vec(0, 0)
  for j, oent in ipairs(ents) do
    if j ~= i then
      local opos = oent.pos
      local r = opos - pos
      local dist2 = r:length_squared()
      local dist = math.sqrt(dist2)
      r = r:normalize()
      c = c - coulomb_force(dist2, r, repulse)
      e = e + elastic_force(dist, r, l_length)
    end
  end
  return c, e*10
end

local dt = 1/200
local n = 0
local damp = 0.92
local r_fac = 0.3

function entity:on_suspended(t)
  print("suspended", t)
end


function entity:on_update()
  n = n + 1
  local epos = Vec(entity:get_position())
  local center = Vec(0,0)
  local hpos = Vec(hero:get_position())
  
  if n % 4 ~= 0 then return end
  
  for i, ent in ipairs(ents) do
    local pos = ent.pos
    center = center+pos
  end
  
  center = center * (1/count)
  -- make barycentre the real center
  --epos = center 
  
  for i, ent in ipairs(ents) do
    local mov = ent :get_movement()
    local pos = ent.pos
    local rpos = Vec(ent:get_position())
    
    -- keep virtual and real pos in sync (mildly)
    pos = rpos * r_fac + pos * (1-r_fac)
    
    local speed = ent.speed 
    
    -- compute crowd electric and elastic forces
    local cf, ef = get_crowd_forces(i, pos)
    
    -- compute driving force
    local df =  driving_force(epos, pos, 10, 1000, 0.99) * 100
    
    -- hero avoid force 
    local hf = hero_force(hpos, pos) * -1600
    
    -- retain forces
    ent.cf, ent.ef, ent.df, ent.hf = cf, ef, df, hf
    
    -- total
    local f = cf*90 +
              --ef +
              df +
              hf
              
    -- damping
    speed = speed*damp
    
    -- add forces
    speed = speed+ f * dt
    
    pos = pos + speed * dt
    ent.pos, ent.speed = pos, speed
    
    if n % 5 ~= 0 then
      mov:set_target(pos.x, pos.y)
      mov:set_speed(100)
    end
  end
  --print("bite")
end

local white = {255,255,255}
local green = {0,255,0}
local red = {255,0,0}
local blue = {0,0,255}
local orange = {255,255,0}

-- draw debug forces
function entity:on_post_draw_disabled(dst)
  
  for i, ent in ipairs(ents) do
    local pos = ent.pos
    local function draw_arrow(dir, magn, color)
      arrow:set_color_modulation(color)
      arrow:set_rotation(dir)
      arrow:set_scale(magn*0.001, 1)
      map:draw_visual(arrow, pos.x, pos.y)
    end
    local speed = ent.speed
    local ef = ent.ef
    local cf = ent.cf
    local df = ent.df
    local hf = ent.hf
    draw_arrow(speed:angle(), speed:length(), white)
    if not ef then return end
    draw_arrow(ef:angle(), ef:length(), green)
    draw_arrow(cf:angle(), cf:length(), red)
    draw_arrow(df:angle(), df:length()*0.1, blue)
    draw_arrow(hf:angle(), hf:length(), orange)
  end
end