#if __VERSION__ >= 130
#define COMPAT_VARYING in
#define COMPAT_TEXTURE texture
out vec4 FragColor;
#else
#define COMPAT_VARYING varying
#define FragColor gl_FragColor
#define COMPAT_TEXTURE texture2D
#endif

#ifdef GL_ES
precision mediump float;
#define COMPAT_PRECISION mediump
#else
#define COMPAT_PRECISION
#endif

uniform sampler2D sol_texture;
uniform bool sol_vcolor_only;
uniform bool sol_alpha_mult;
uniform int sol_time;
COMPAT_VARYING vec2 sol_vtex_coord;
COMPAT_VARYING vec4 sol_vcolor;

vec3 unpack_normal() {
    vec4 pack = COMPAT_TEXTURE(sol_texture, sol_vtex_coord+vec2(0.0,0.5));
    return pack.xyz * 2.0 - vec3(1.0);
}

vec3 light_dir() {
    float time = float(sol_time)*0.005;
    return normalize(vec3(sin(time), cos(time), 1));
}

vec4 illum() {
    vec3 normal = unpack_normal();
    vec3 light = light_dir();
    float fac = max(0.0, dot(normal, light));
    vec3 col =  fac* vec3(1.0,1.0,0.5);
    return vec4(col, 1.0);
}

void main() {
    if(!sol_vcolor_only) {
      vec4 tex_color = COMPAT_TEXTURE(sol_texture, sol_vtex_coord);
      //vec4 tex_color = COMPAT_TEXTURE(sol_texture, sol_vtex_coord+vec2(0.0,0.5));
      FragColor = tex_color * sol_vcolor * illum();
      if(sol_alpha_mult) {
        FragColor.rgb *= sol_vcolor.a; //Premultiply by opacity too
      }
    } else {
      FragColor = sol_vcolor;
    }
}
