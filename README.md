# Solarus Crowds

This repo contains a very informal test of crowd simulation with the solarus engine.

The only working map is `first_map`. 

The actual crowd generation and simulation is inside the `entities/crowd.lua` custom entity.

The code style is really quick and dirty and not exactly meant for transmission.

Please ignore the `array.lua` file in `scripts`. It is a skeleton of 
luajit only multidimensionnal array implementation that was aimed at accelerating
the N^2 complex force calculation made trough the `vector.lua` class which seem to be the performance
bottleneck of this experiment.

After some research, using `numlua` to do the forces calculations could accelerate greatly the performance.

The use of some acceleration structure like a quad tree "à la" Barnes-Hut could also lead to
nive speed improvement in larger scenes.

Feel free to modify according to your needs. Credit is appreciated but far from mandatory
given the clunky quality of the code...

# Licenses

Most of the default Solarus helper code is under the GPLv3 license.

The files of the experiment in itself are under the more permissive MIT license.
